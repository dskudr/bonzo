FROM python:3
WORKDIR /code
ADD ./bonzo /code/
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
# RUN sudo apt install -y python3-venv
# RUN python3 -m venv venv
# RUN source venv/bin/activate
RUN pip install -r requirements.txt
EXPOSE 8000
CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]
